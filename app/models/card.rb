class Card < ApplicationRecord
  # Sets the model attributes from a JSON string. Returns self.
  include ActiveModel::Serializers::JSON
  
  belongs_to :list
  has_many :comments, dependent: :destroy
  validates_presence_of :title, :list_id
  
  delegate :board_id, to: :list
  
  def attributes
    super.merge("board_id" => board_id, "comments_count" => comments_count)
  end
  
  def comments_count
    comments.count
  end
end
