export default function boardsReducer(state = [], action) {
  if (action.type === 'FETCH_BOARDS_SUCCESS') {
    return action.boards;
  } else if (action.type === 'CREATE_BOARD_SUCCESS') {
    const newBoard = action.board;
    newBoard.id = Number(newBoard.id);

    return state.concat(newBoard);
  } else if (action.type === 'FETCH_BOARD_SUCCESS') {
    // the outcome we need here is for the fetched board to replace any old version 
    // of itself in the store, while retaining any other boards that are also in the store
    const otherBoards = state.filter(board => board.id !== action.board.id);
    const { lists, ...currentBoardWithoutLists } = action.board;
    
    // console.log(otherBoards);
    // console.log(currentBoardWithoutLists);
    // console.log(otherBoards.concat(currentBoardWithoutLists));
    return otherBoards.concat(currentBoardWithoutLists);
  } else {
    return state;
  }
}
