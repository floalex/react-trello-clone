import {
  CREATE_LIST_SUCCESS,
  FETCH_BOARD_SUCCESS,
  UPDATE_LIST_SUCCESS,
} from '../constants/ActionTypes';

export default function ListsReducer(state=[], action) {
  switch(action.type) {
    case FETCH_BOARD_SUCCESS:
      const boardLists = action.board.lists.map(list => {
        const { cards, ...boardListsWithoutcards } = list;
        return boardListsWithoutcards;
      });
      
      return boardLists;      
      // const boardListsIds = boardLists.map(list => list.id);
      
      // const otherLists = state.filter(
      //   list => boardListsIds.indexOf(list.id) === -1
      // );
    
      // return otherLists.concat(boardLists);
    case CREATE_LIST_SUCCESS:
      return state.concat(action.list);
    case UPDATE_LIST_SUCCESS:
      // names in const need to be matched with the params passed in action
      const {listId, updatedListlist} = action;
  
      const editedList = state.map(list => {
        if (listId === list.id) {
          return updatedListlist;
        } else {
          return list;
        }
      });
      
      return editedList;
    default:
      return state;
  }
}

