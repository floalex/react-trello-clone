export const BOARDS_INDEX_URL = '/api/boards';
export const CREATE_BOARD_URL = '/api/boards';

export const boardURL = (boardId) => `/api/boards/${boardId}`;  //be sure to use ``

export const CREATE_LIST_URL = '/api/lists';

export const updateListURL = (listId) => `/api/lists/${listId}`;
