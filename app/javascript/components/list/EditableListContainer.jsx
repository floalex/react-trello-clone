import React from 'react';
import PropTypes from 'prop-types';

import EditableList from './EditableList';

import * as listActions from '../../actions/ListActions';

class EditableListContainer extends React.Component {
  static contextTypes = {
    store: PropTypes.object
  };

  static propTypes = {
    list: PropTypes.object
  };

  state = {
    showForm: false,
    title: this.props.list.title
  };

  handleTileClick = (e) => {
    this.setState({
      showForm: true
    });
  }
  
  handleChange = (e) => {
    this.setState({
      title: e.target.value
    });
  }
  
  // Blur should be used as need to save title when user clicks away
  handleBlur = (e) => {
    if (e.target.value !== this.props.list.title) {
      const store = this.context.store;
      store.dispatch(
        listActions.updateList(
          Number(this.props.list.id), 
          { title: this.state.title },
          () => {
            // reset default state
            this.setState({
              showForm: false
            });
          }
        )
      );
    } else {
      this.setState({
        showForm: false
      });
    }
  }
  
  handleKeyPress = (e) => {
    if (e.key === "Enter") { e.target.blur(); }
  }

  render() {
    return (
      <EditableList
        showForm={this.state.showForm}
        title={this.state.title}
        onTitleClick={this.handleTileClick}
        onChange={this.handleChange}
        onBlur={this.handleBlur}
        onKeyPress={this.handleKeyPress}
      />
    );
  }
  
}


export default EditableListContainer;