import React from 'react';
import PropTypes from 'prop-types';

import EditableListContainer from './EditableListContainer';

const ListContainer = (props) => (
  <div 
    className="list-wrapper"
    data-list-id={props.list.id}
  >
    <div className="list-background">
      <div className="list">
        <a className="more-icon sm-icon" href=""></a>
        <EditableListContainer list={props.list}/>
        <div className="add-dropdown add-top">
          <div className="card"></div>
          <a className="button">Add</a><i className="x-icon icon"></i>
          <div className="add-options"><span>...</span>
          </div>
        </div>
        <div
          className="add-card-toggle"
          data-position="bottom"
        >Add a card...</div>
      </div>
    </div>
  </div>
);

ListContainer.propTypes = {
  cards: PropTypes.array,
  list: PropTypes.object.isRequired,
};

export default ListContainer;