import React from 'react';
import PropTypes from 'prop-types';

const EditableList = (props) => {
  if (props.showForm) {
    return (
      <div>
        <input 
          type="text"
          className="list-title"
          value={props.title}
          onChange={props.onChange}
          onKeyPress={props.onKeyPress}
          onBlur={props.onBlur}
          autoFocus={true}
        />
      </div>
    );
  } else {
    return (
      <div>
        <p 
          className="list-title"
          onClick={props.onTitleClick}
        >
        {props.title}
        </p>
      </div>
    );
  }
};

EditableList.propTypes = {
  showForm: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onTitleClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
};

export default EditableList;