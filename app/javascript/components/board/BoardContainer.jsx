import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import * as actions from '../../actions/BoardActions';

import * as statuses from '../../constants/Statuses';

import Board from './Board';

class BoardContainer extends React.Component {
  static contextTypes = {
    store: PropTypes.object.isRequired
  };
  
  static childContextTypes = {
    currentBoardId: PropTypes.number
  };
  
  static propTypes = {
    match: PropTypes.object
  };
  
  getChildContext() {
    return {
      currentBoardId: this.boardId()
    };
  }
  
  constructor(props) {
    super(props);
    this.state = {
      board: null,
      isFetching: false
    };
  }
  
  // set the board and subscribe changes
  componentDidMount() {
    const store = this.context.store;
    this.unsubscribe = store.subscribe(() => this.updateBoardInState());
    
    // still need to fetch board data when we first visit it
    this.updateBoardInState();
  }
  
  componentWillUnmount() {
    this.unsubscribe;
  }
  
  // get id from url and turn it into number
  boardId = () => {
    const { id } = this.props.match.params;
    const { url } = this.props.match;
    
    if (url.match(new RegExp("^/boards/"))) {
      return Number(id);
    } else {
      return null;
    }
  }
  
  updateBoardInState = () => {
    const boardId = this.boardId();
    
    if (!boardId) { return null; }
    
    // fetch board data with boardId
    if (!this.state.board && !this.state.isFetching) {
      this.fetchBoard(boardId);
    }
  }
  
  // change the store by fetching specific board with id
  fetchBoard = (id) => {
    const store = this.context.store;
    this.setState({
      isFetching: true
    }, () => {
      store.dispatch(actions.fetchBoard(id, this.doneFetchingBoard));
    });
  }
  
  // set board when done fetching
  doneFetchingBoard = (board) => {
    this.setState({
      isFetching: false,
      board
    });
  };
  
  render() {
    return (
      <Board board={this.state.board} />
    );
  }
}

export default BoardContainer;
