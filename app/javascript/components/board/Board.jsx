import React from 'react';
import PropTypes from 'prop-types';

import ExistingListsContainer from './ExistingListsContainer';
import CreateListTileContainer from './CreateListTileContainer';

const Board = (props) => {
  if (props.board) {
    return (
      <div>
        <header>
          <ul>
            <li id="title">{props.board.title}</li>
            <li className="star-icon icon"></li>
            <li className="private private-icon icon">Private</li>
          </ul>
          <div className="menu">
            <i className="more-icon sm-icon"></i>Show Menu</div>
          <div className="subscribed">
            <i className="sub-icon sm-icon"></i>Subscribed</div>
        </header>
        <main>
          <div id="list-container" className="list-container">
            <ExistingListsContainer 
              boardId={props.board.id}
            />
            <CreateListTileContainer />
          </div>
        </main>
      </div>
    );
  } else {
    return null;
  }
};

Board.propTypes = {
  board: PropTypes.object
};

export default Board;