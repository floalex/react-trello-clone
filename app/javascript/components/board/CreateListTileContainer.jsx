import React from 'react';
import PropTypes from 'prop-types';

// grab the current board id using this.props.match.params.id
import { withRouter } from 'react-router-dom';

import * as listActions from '../../actions/ListActions';

import CreateListTile from './CreateListTile';

class CreateListTileContainer extends React.Component {
  state = {
    showForm: false,
    title: ''
  };
  
  static contextTypes = {
    store: PropTypes.object
  };

  static propTypes = {
    match: PropTypes.object
  };
  
  handleTileClick = (e) => {
    e.stopPropagation();
    
    this.setState({
      showForm: true
    });
  }
  
  handleCloseClick = (e) => {
    e.stopPropagation();
    
    this.setState({
      showForm: false
    });
  }
  
  handleChange = (e) => {
    this.setState({
      title: e.target.value
    });
  }
  
  handleSubmit = (e) => {
    e.preventDefault();
    
    const boardId = Number(this.props.match.params.id);
    const store = this.context.store;
    
    // create list by passing board id and title; dispatch changes to store
    store.dispatch(
      listActions.createList(
        boardId, { title: this.state.title },
        () => {
          // reset default state
          this.setState({
            title: '',
            showForm: false
          });
        }
      )
    );
  }

  render() {
    return(
      <CreateListTile
        showForm={this.state.showForm}
        title={this.state.title}
        onTileClick={this.handleTileClick}
        onCloseClick={this.handleCloseClick}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
      />
    );
  }
}

export default withRouter(CreateListTileContainer);