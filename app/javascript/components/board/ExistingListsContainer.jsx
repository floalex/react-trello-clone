import React from 'react';
import PropTypes from 'prop-types';

// import * as listSelectors from '../../selectors/ListSelectors';

import * as cardActions from '../../actions/CardActions';

import ListContainer from './../list/ListContainer';

class ExistingListsContainer extends React.Component {
  // set the default state for new card with corresponding list id and empty title
  state = {
    addCardActiveListId: null,
    newCardFormTitle: '',
  }
  
  // propTypes 
  static propTypes = {
    boardId: PropTypes.number.isRequired
  }
  
  static contextTypes = {
    store: PropTypes.object.isRequired
  }
  
  static propTypes = {
    match: PropTypes.object
  };
  
  handleTileClick = (e) => {
    e.stopPropagation();
    
    this.setState({
      showForm: true
    });
  }
  
  handleCloseClick = (e) => {
    e.stopPropagation();
    
    this.setState({
      showForm: false
    });
  }
  
  handleChange = (e) => {
    this.setState({
      title: e.target.value
    });
  }
  
  handleSubmit = (e) => {
    e.preventDefault();
    
    const boardId = Number(this.props.match.params.id);
    const store = this.context.store;
    
    store.dispatch(
      cardActions.createList(
        boardId, { title: this.state.title },
        () => {
          // reset default state
          this.setState({
            title: '',
            showForm: false
          });
        }
      )
    );
  }
  
  // need to get the view changed after adding/changing new lists
  getLists = () => {
    return this.context.store.getState().lists;
  }
  
  render() {
    const currLists = this.getLists();
    {/* map each list's attr to props, pass to ListContainer*/}
    const lists = currLists.map(list => (       
      <ListContainer
        key={list.id}
        list={list}
        addCardList
        newCardForm
        onAddCardClick={this.handleAddCardClick}
        onAddCardClose={this.handleNewCardFormCloseClick}
        onNewCardFormChange={this.handleNewCardFormChange}
        onNewCardFormSubmit={this.handleNewCardFormSubmit}
        onNewCardFormKeyPress
      />
    ));
    
    return (
      <div 
        id="existing-lists"
        className="existing-lists"
      >
      {lists}
      </div>
    );
  }
}

export default ExistingListsContainer;