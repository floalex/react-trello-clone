json.merge! @board.attributes 

json.lists(@board.lists) do |list|
  json.merge! list.attributes
  
  json.cards(list.cards) do |card|
    json.id card.id
    json.list_id card.list_id
    json.title card.title
    json.description card.description
  end
end