class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards do |t|
      t.string :title, nil: false
      t.text :description
      t.integer :list_id, nil: false
      t.string :labels, default: [], array: true
      t.datetime :due_date
      t.timestamps
    end
  end
end
