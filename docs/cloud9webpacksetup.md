application.html.haml (or erb): add to your %head
`= javascript_pack_tag 'application'`
change in webpacker.yml (change nothing else):
development:
  <<: *default

  dev_server:
    host: '<my_app_name>.c9users.io'
    port: 8081 # Some unused port
    https: false # is FALSE!
Start WebPacker server with:
bin/webpack-dev-server --public <my_app_name>.c9users.io:8081
=> You can now check if it's working under: (note: httpS)

https://<my_app_name>.c9users.io:8081/packs/application.js

You should see some javascript

Start your rails server as usual

Unfortunately (in this case) your browser will probably block "mixed content"; in chrome you have to manually allow it on this site by clicking on the "shield" icon in your navigation bar (omnibox). Now the following should show up in your browser's console:

Hello World from Webpacker