## Create BoardView with Existing Lists
1. Needs `show` method in Rails controller
2. Define Rails routes for API, remember also needs to create show json under view, need to use merge
3. Define route in Application class
4. Build a board container to featch specific board
5. Notice that although we use a flat data structure to store lists and board like this:
  ```
    {
    boards: [{
      id: 1
    },
    lists: [{
      id: 1,
      board_id: 1
    }]
  }
  ```
  We define the one-to-manay relationship for Board and List so we can get all the lists belong to a board. 
  In "show.json.jbuilder", we are able to present it in a nested way with `json.lists(@board.lists) do |list|`
6. Subcribe changes on board, then need `getState()` to get the updated changes if any part of the board has been changed.
  Mistake I have made: At first I just passed `this.props.board.lists` for ListContainer component, but view didn't changed
  after adding new list even the reducer and API set up correctly. Therefore need to get the updated lists from store first and 
  pass it to render method in ListContainer.
  ```
  getLists = () => {
    return this.context.store.getState().lists;
  }
  
  render() {
    const currLists = this.getLists();
    {/* map each list's attr to props, pass to ListContainer*/}
    const lists = currLists.map(list => (
  ....other codes
  ```
  In other words, in order for `subscribe` works(without `Connect`), need to call `store.getState()` to get the updated data
  
## General Workflow for updating list title
```
Backend: Add update route in list -> Add update method in controller -> add jbuilder for update in view
Frontend: Add container componenet to hold the state(for update states) -> Add presentationl form -> Add action for update 
          ->Add reducer for update 
```

Debug acxios:
```
return axios.put('users/12345')
  .catch(function (error) {
    if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  });
```